FROM ubuntu:focal as builder

# Install dependencies
# We need uhd so enb and ue are built
# Use curl and unzip to get a specific commit state from github
RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
     cmake \
     libuhd-dev \
     uhd-host \
     libboost-program-options1.71-dev \
     libvolk2-dev \
     libfftw3-dev \
     libmbedtls-dev \
     libsctp-dev \
     libconfig++-dev \
     libzmq3-dev \
     libczmq-dev \
     curl \
     unzip \
 && rm -rf /var/lib/apt/lists/*

# Pinned git commit used for this example
ARG COMMIT=c892ae56be5302eaee5ca00e270efc7a5ce6fbb2

# Download and build
RUN curl -LO https://github.com/srsLTE/srsLTE/archive/${COMMIT}.zip \
 && unzip ${COMMIT}.zip \
 && rm ${COMMIT}.zip \
 && mv srsLTE-${COMMIT} srsLTE

WORKDIR /srsLTE/build

RUN cmake -DCMAKE_BUILD_TYPE=Release ../ \
 && make -j$(nproc) \
 && make install \
 # Install examples
 && find ./lib/examples -type f -not -path './lib/examples/Makefile' -not -path './lib/examples/cmake*' -not -path './lib/examples/CTest*' -not -path './lib/examples/CMake*' -not -path './lib/examples/test/*' -exec cp --verbose '{}' /usr/local/bin \;

# Final container
FROM ubuntu:focal

# Runtime dependencies
RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
     libuhd3.15.0 \
     uhd-host \
     libboost-program-options1.71.0 \
     libvolk2-bin \
     libfftw3-bin \
     libmbedtls12 \
     libsctp1 \
     libconfig++ \
     libczmq4 \
     iputils-ping \
     iproute2 \
     iptables \
     # BEGIN Debugging tools
     curl \
     unzip \
     net-tools \
     dnsutils \
     ncat \
     tcpdump \
     less \
     mc \
     # END Debugging tools
 && rm -rf /var/lib/apt/lists/*

# Copy srsLTE distribution
COPY --from=builder /usr/local /usr/local

RUN ldconfig \
 # Install configs into /etc/srslte/
 && srslte_install_configs.sh service

WORKDIR /srsLTE

# Run commands with line buffered standard output
# (-> get log messages in real time)
ENTRYPOINT [ "stdbuf", "-o", "L" ]
